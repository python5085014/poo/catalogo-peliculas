from model.Pelicula import Pelicula
from service.Catalogo import Catalogo


def selection(valor):
    if valor == 1:
        nombrePelicula = input("Ingrese el nombre de la pelicula por favor")
        duracion = int(input("Ingrese se duracion en minutos"))
        Catalogo.agregarPelicula(Pelicula(nombrePelicula, duracion))
    elif valor == 2:
        Catalogo.listarPeliculas()
    elif valor == 3:
        Catalogo.eliminarPelicula()


def iniciar():
    option = None
    while option != 4:
        try:
            print("Opciones: ")
            print("1. Agregar Pelicula")
            print("2. Listar Pelicula")
            print("3. Eliminar Catalogo de Peliculas")
            print("4. Salir")
            option = int(input("Escribe tu opción [1-4]"))
            selection(option)
        except Exception as e:
            print(f'Ocurrio un error {e}')
            option = None
    else:
        print("Salimos del programa")


iniciar()