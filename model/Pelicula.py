class Pelicula:
    def __init__(self,nombre,duracion):
        self._nombre = nombre
        self._duracion = duracion

    def __str__(self):
        return f'Pelicula: {self._nombre} duración: {self._duracion}'

    # Métodos getters
    @property
    def nombre(self):
        return self._nombre

    @property
    def duracion(self):
        return self._duracion

    # Métodos setters

    @nombre.setter
    def nombre(self,nombre):
        self.nombre = nombre

    @duracion.setter
    def duracion(self,duracion):
        self.duracion = duracion