import os


class Catalogo:
    ruta = "prueba.txt"

    @classmethod
    def agregarPelicula(cls, pelicula):
        with open(cls.ruta, "a", encoding="utf8") as file:
            file.write(f'{pelicula.nombre},{pelicula.duracion} \n')

    @classmethod
    def listarPeliculas(cls):
        with open(cls.ruta, "r", encoding="utf8") as file:
            print("Catálogo de peliculas".center(50, "-"))
            print(file.read())

    @classmethod
    def eliminarPelicula(cls):
        os.remove(cls.ruta)
        print("Archivo eliminado...")


"""
cls: se utiliza cuando el método pertenece a la clase, mediante `cls`
se accede por el nombre de la clase.
 
"""
